<?php

/**
 * This file is part of the "psvneo_gated_content" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace PsvNeo\PsvneoGatedContent\Controller;

use InvalidArgumentException;
use PsvNeo\PsvneoGatedContent\Domain\Model\Request;
use PsvNeo\PsvneoGatedContent\Domain\Repository\RequestRepository;
use PsvNeo\PsvneoGatedContent\Domain\Service\FluidTemplateEmailServiceInterface;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class GatedContentController extends ActionController
{
    /**
     * @var RequestRepository
     */
    protected $gatedContentRequestRepository;

    /**
     * @var FluidTemplateEmailServiceInterface
     */
    protected $fluidTemplateEmailService;

    /**
     * @var ResourceFactory
     */
    protected $resourceFactory;

    public function injectGatedContentRequestRepository(RequestRepository $gatedContentRequestRepository): void
    {
        $this->gatedContentRequestRepository = $gatedContentRequestRepository;
    }

    public function injectFluidTemplateEmailServiceInterface(
        FluidTemplateEmailServiceInterface $fluidTemplateEmailService
    ): void {
        $this->fluidTemplateEmailService = $fluidTemplateEmailService;
    }

    public function injectResourceFactory(ResourceFactory $resourceFactory): void
    {
        $this->resourceFactory = $resourceFactory;
    }

    public function indexAction(): void
    {
    }

    public function requestFileAction(Request $gatedContentRequest, int $fileUid): void
    {
        // Validate request.
        if ($fileUid != (int) $this->settings['file']) {
            return;
        }
        if (!$gatedContentRequest->isAgreedPrivacyPolicy()) {
            throw new InvalidArgumentException('User must agree to privacy policy!', 1579089816);
        }

        // Fetch file by settings.
        $file = $this->resourceFactory->getFileObject((int) $this->settings['file']);
        /** @var Context $context */
        $context = GeneralUtility::makeInstance(Context::class);
        $currentFrontendUserUid = $context->getPropertyFromAspect('frontend.user', 'id');

        // Enrich and persist gated content request.
        $gatedContentRequest->setFrontendUserUid($currentFrontendUserUid);
        $gatedContentRequest->setFilePath($file->getPublicUrl());
        $this->gatedContentRequestRepository->add($gatedContentRequest);

        // Send email.
        $this->fluidTemplateEmailService->send(
            [$this->settings['email']['receiver']],
            [$this->settings['email']['sender']['email'] => $this->settings['email']['sender']['name']],
            $this->settings['email']['subject'],
            $this->settings['email']['template'],
            [
                'gatedContentRequest' => $gatedContentRequest,
            ]
        );

        $this->view->assignMultiple([
            'file' => $file,
            'publicPath' => $file->getPublicUrl(),
        ]);
    }
}
