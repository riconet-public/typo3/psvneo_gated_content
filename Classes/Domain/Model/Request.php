<?php

/**
 * This file is part of the "psvneo_gated_content" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace PsvNeo\PsvneoGatedContent\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

class Request extends AbstractEntity
{
    /**
     * @var string|null
     */
    protected $name = '';

    /**
     * @var string|null
     */
    protected $company = '';

    /**
     * @var string|null
     */
    protected $email = '';

    /**
     * @var string|null
     */
    protected $filePath = '';

    /**
     * @var int|null
     */
    protected $frontendUserUid = 0;

    /**
     * @var bool|null
     */
    protected $agreedToDataSecurity = false;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): void
    {
        $this->company = $company;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    public function setFilePath(?string $filePath): void
    {
        $this->filePath = $filePath;
    }

    public function getFrontendUserUid(): ?int
    {
        return $this->frontendUserUid;
    }

    public function setFrontendUserUid(?int $frontendUserUid): void
    {
        $this->frontendUserUid = $frontendUserUid;
    }

    public function isAgreedPrivacyPolicy(): ?bool
    {
        return $this->agreedToDataSecurity;
    }

    public function setAgreedToDataSecurity(?bool $agreedToDataSecurity): void
    {
        $this->agreedToDataSecurity = $agreedToDataSecurity;
    }
}
