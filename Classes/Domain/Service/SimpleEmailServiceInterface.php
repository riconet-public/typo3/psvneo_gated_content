<?php

/**
 * This file is part of the "psvneo_gated_content" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace PsvNeo\PsvneoGatedContent\Domain\Service;

interface SimpleEmailServiceInterface
{
    public function send(
        array $recipient,
        array $sender,
        string $subject,
        string $body,
        string $contentType = 'text/html'
    ): bool;
}
