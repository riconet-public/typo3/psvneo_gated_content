<?php

/**
 * This file is part of the "psvneo_gated_content" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace PsvNeo\PsvneoGatedContent\Domain\Service;

class FluidTemplateEmailService implements FluidTemplateEmailServiceInterface
{
    /**
     * @var SimpleEmailServiceInterface
     */
    protected $emailService;

    /**
     * @var FluidTemplateParserServiceInterface
     */
    protected $fluidTemplateParserService;

    public function __construct(
        SimpleEmailServiceInterface $emailService,
        FluidTemplateParserServiceInterface $fluidTemplateParserService
    ) {
        $this->emailService = $emailService;
        $this->fluidTemplateParserService = $fluidTemplateParserService;
    }

    public function send(
        array $recipient,
        array $sender,
        string $subject,
        string $templateFilePath,
        array $variables = [],
        string $contentType = 'text/html'
    ): bool {
        $body = $this->fluidTemplateParserService->parseFile($templateFilePath, $variables);

        return $this->emailService->send(
            $recipient,
            $sender,
            $subject,
            $body,
            $contentType
        );
    }
}
