<?php

/**
 * This file is part of the "psvneo_gated_content" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace PsvNeo\PsvneoGatedContent\Domain\Service;

use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class SimpleEmailService implements SimpleEmailServiceInterface
{
    public function send(
        array $recipient,
        array $sender,
        string $subject,
        string $body,
        string $contentType = 'text/html'
    ): bool {
        /** @var MailMessage $message */
        $message = GeneralUtility::makeInstance(MailMessage::class);
        $message->setTo($recipient);
        $message->setFrom($sender);
        $message->setSubject($subject);
        'text/html' === $contentType ? $message->setBody()->html($body) : $message->setBody()->text($body);
        $message->send();

        return $message->isSent();
    }
}
