<?php

/**
 * This file is part of the "psvneo_gated_content" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace PsvNeo\PsvneoGatedContent\Domain\Service;

use TYPO3\CMS\Fluid\View\StandaloneView;

class FluidTemplateParserService implements FluidTemplateParserServiceInterface
{
    public function parseFile(string $filePath, array $variables = []): string
    {
        $view = new StandaloneView();
        $view->setTemplatePathAndFilename($filePath);
        $view->assignMultiple($variables);

        return trim($view->render());
    }
}
