<?php

/**
 * This file is part of the "psvneo_gated_content" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

return (function ($extensionKey, $table) {
    $ll = "LLL:EXT:$extensionKey/Resources/Private/Language/locallang_db.xlf";
    $fields = 'company,name,email,file_path,frontend_user_uid,agreed_to_data_security';

    return [
        'ctrl' => [
            'title' => "$ll:$table",
            'label' => 'uid',
            'iconfile' => "EXT:$extensionKey/Resources/Public/Icons/Extension.svg",
            'tstamp' => 'tstamp',
            'crdate' => 'crdate',
            'searchFields' => "$fields",
            'default_sortby' => 'ORDER BY uid',
        ],
        'types' => [
            '1' => [
                'showitem' => "$fields",
            ],
        ],
        'columns' => [
            'name' => [
                'exclude' => true,
                'label' => "$ll:$table.name",
                'config' => [
                    'type' => 'input',
                    'size' => 5,
                    'eval' => 'trim',
                    'readOnly' => true,
                ],
            ],
            'company' => [
                'exclude' => true,
                'label' => "$ll:$table.company",
                'config' => [
                    'type' => 'input',
                    'size' => 5,
                    'eval' => 'trim',
                    'readOnly' => true,
                ],
            ],
            'email' => [
                'exclude' => true,
                'label' => "$ll:$table.email",
                'config' => [
                    'type' => 'input',
                    'size' => 5,
                    'eval' => 'trim,required',
                    'readOnly' => true,
                ],
            ],
            'file_path' => [
                'exclude' => true,
                'label' => "$ll:$table.file_path",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim,required',
                    'readOnly' => true,
                ],
            ],
            'frontend_user_uid' => [
                'exclude' => true,
                'label' => "$ll:$table.frontend_user_uid",
                'config' => [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'items' => [
                        ['---', 0],
                    ],
                    'default' => 0,
                    'foreign_table' => 'fe_users',
                    'readOnly' => true,
                ],
            ],
            'agreed_to_data_security' => [
                'exclude' => true,
                'label' => "$ll:$table.agreed_to_data_security",
                'config' => [
                    'type' => 'check',
                    'default' => false,
                    'readOnly' => true,
                ],
            ],
        ],
    ];
})('psvneo_gated_content', 'tx_psvneogatedcontent_domain_model_request');
