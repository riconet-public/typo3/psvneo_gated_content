<?php

/**
 * This file is part of the "psvneo_gated_content" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

defined('TYPO3_MODE') || die();

(function ($extensionKey) {
    $ll = "LLL:EXT:$extensionKey/Resources/Private/Language/locallang_db.xlf";
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        $extensionKey,
        'GatedContent',
        "$ll:plugin.gated_content"
    );
    \PsvNeo\PsvneoGatedContent\Utility\PluginUtility::registerFlexForm(
        $extensionKey,
        'Configuration/FlexForm/GatedContent.xml',
        'GatedContent'
    );
})('psvneo_gated_content');
