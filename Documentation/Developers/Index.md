[Documentation](../Index.md)/`Developers`

# For developers

## How to run local tests
| Description       | command                     |
|-------------------|-----------------------------|
| Build the project | `composer build`            |
| Quality tools     | `composer quality`          |
| Lint PHP          | `composer lint-php`         |
| Lint Typoscript   | `composer lint-typoscript`  |
| Find debugs       | `composer find-debugs`      |
| Unit tests        | `composer unit-tests`       |
| Functional tests  | `composer functional-tests` |
