# Documentation for the extension "Reference portal"

## Navigation
- [Installation](Installation/Index.md)
- [Configuration](Configuration/Index.md)
- [For editors](Editors/Index.md)
- [For developers](Developers/Index.md)
