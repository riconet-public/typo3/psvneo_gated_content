<?php
/*
 * This file is part of the "psvneo_gated_content" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

$EM_CONF['psvneo_gated_content'] = [
    'title' => 'Gated content',
    'description' => 'This extension adds a simple way to handle "gated content".',
    'version' => '2.0.1',
    'category' => 'plugin',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-10.4.99',
            'typoscript_rendering' => '2.3.0-2.3.99',
        ]
    ],
    'state' => 'stable',
    'uploadfolder' => false,
    'clearCacheOnLoad' => true,
    'author' => 'Wolf',
    'author_email' => 'w.utz@psv-neo.de',
    'author_company' => 'PSVneo',
    'autoload' => [
        'psr-4' => [
            'PsvNeo\\PsvneoGatedContent\\' => 'Classes',
        ]
    ],
    'autoload-dev' => [
        'psr-4' => [
            'PsvNeo\\PsvneoGatedContent\\Tests\\' => 'Tests',
        ]
    ],
];
