<?php

defined('TYPO3_MODE') || die();

(function ($extensionKey) {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        "PsvNeo.$extensionKey",
        'GatedContent',
        [
            'GatedContent' => 'index, requestFile',
        ],
        [
            'GatedContent' => 'requestFile',
        ]
    );
})('psvneo_gated_content');
