# TYPO3 Extension - Gated content
This extension adds a simple way to handle "gated content".

## Requirements
- TYPO3 9.5

## Documentation
You can read the documentation [here](Documentation/Index.md).

# Credits
* Extension icon by [Freepik](https://www.flaticon.com/de/autoren/freepik)
