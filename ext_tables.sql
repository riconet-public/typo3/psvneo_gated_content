CREATE TABLE tx_psvneogatedcontent_domain_model_request (
    uid int(11) UNSIGNED DEFAULT 0 NOT NULL AUTO_INCREMENT,
    pid int(11) DEFAULT 0 NOT NULL,
    tstamp int(11) UNSIGNED DEFAULT 0 NOT NULL,
    crdate int(11) UNSIGNED DEFAULT 0 NOT NULL,
    company varchar(255) DEFAULT '' NOT NULL,
    name varchar(255) DEFAULT '' NOT NULL,
    email varchar(255) DEFAULT '' NOT NULL,
    file_path varchar(255) DEFAULT '' NOT NULL,
    frontend_user_uid int(11) UNSIGNED DEFAULT 0 NOT NULL,
    agreed_to_data_security tinyint(2) UNSIGNED DEFAULT 0 NOT NULL,
    PRIMARY KEY (uid),
    KEY parent (pid)
);
