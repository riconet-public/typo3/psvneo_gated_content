;(function ($, axios) {
  'use strict'

  $.fn.gatedContentRequestFile = function () {
    var $elements = this
    if ($elements.length === 0) {
      return
    }
    $elements.submit(function (event) {
      event.preventDefault()
      var form = event.target
      var fileUid = form.dataset.gatedContentFile
      axios({
        method: 'post',
        url: form.dataset.ajaxUrl,
        data: new FormData(form),
        headers: {'Content-Type': 'multipart/form-data' }
      })
      .then(function (response) {
        console.log(fileUid)
        $('*[data-gated-content-container="'+fileUid+'"]').html(response.data)
        $('.modal').modal('hide');
      })
      .catch(function (response) {
        $('.modal').modal('hide');
        console.error(response);
      });
    })
  }
}(jQuery, axios))
$('form[data-gated-content-form]').gatedContentRequestFile()